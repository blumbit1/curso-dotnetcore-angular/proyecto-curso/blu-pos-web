import { Component, OnInit } from '@angular/core';
import { CategoryService } from '@shared/services/category.service';
import { CustomTitleService } from '@shared/services/custom-title.service';
import { fadeInRight400ms } from 'src/@vex/animations/fade-in-right.animation';
import { scaleIn400ms } from 'src/@vex/animations/scale-in.animation';
import { stagger40ms } from 'src/@vex/animations/stagger.animation';
import { componentSettings } from './category-list-config';
import { CategoryApi } from 'src/app/responses/category/category.response';

@Component({
  selector: 'vex-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss'],
  animations: [
    stagger40ms,
    scaleIn400ms,
    fadeInRight400ms
  ]
})
export class CategoryListComponent implements OnInit {

  // Variable
  component

  constructor(
    customTitle: CustomTitleService,
    public _categoryService: CategoryService
  ) { 
    customTitle.set('Categorias');
  }

  ngOnInit(): void {
    this.component = componentSettings
  }

  // Adicionar un metodo para seleccionar una fila en la tabla de categorias
  rowClick(e: any) {
    let action = e.action;
    let category = e.row
    
    switch(action) {
      case 'edit':
        this.CategoryEdit(category);
        break;
      case 'remove':
        this.CategoryRemove(category);
        break;
    }
    return false;
  }

  CategoryEdit(row: CategoryApi) {}
  CategoryRemove(category: any) {}

}
