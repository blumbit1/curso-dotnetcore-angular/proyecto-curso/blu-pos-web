import { convertDateToRequest } from '@shared/functions/helpers'
import { params } from 'src/app/commons/params-api.interface'

export class ListCategoryRequest extends params {

    // Constructor
    constructor(
        numPage: number,
        order: 'desc' | 'asc',
        sort: string,
        records: 10 | 20 | 50,
        numFilter: number= 0,
        textFilter: string = '',
        stateFilter: number = 0,
        private startDate: string, //fechaCreacionIni
        private endDate: string    //fechaCreacionFin
    ) {
        // Dentro del constructor inicializaremos lo que se encuentra en el Constructor
        super(
            true,
            numPage,
            order,
            sort,
            records,
            false,
            numFilter,
            textFilter,
            stateFilter
        )
        // Utilizaremos el Helper para convertir las fechas quew estan en texto
        // al formato respectivo(Date)
        this.startDate = convertDateToRequest(this.startDate, 'date');
        this.endDate = convertDateToRequest(this.endDate, 'date');
    }

}