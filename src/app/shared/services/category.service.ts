import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertService } from './alert.service';
import { Observable } from 'rxjs';
import { environment as env } from 'src/environments/environment';

import { CategoryApi } from 'src/app/responses/category/category.response';
import {endpoint } from '@shared/apis/endpoint';
import { ListCategoryRequest } from 'src/app/requests/category/category.request';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private _http: HttpClient, private _alert: AlertService) { }

  // Obtener los datos desde backend de todas las Categorias
  GetAll(size, sort, order, page, getInputs): Observable<CategoryApi> {

    //`Se estima que la Tierra tiene ${edad} mil millones de años.`
    const requestUrl = `${env.api}${endpoint.LIST_CATEGORIES}`;
    const params: ListCategoryRequest = new ListCategoryRequest(
      page + 1,
      order,
      sort,
      size,
      getInputs.numFilter,
      getInputs.textFilter,
      getInputs.stateFilter,
      getInputs.startDate,
      getInputs.endDate
    )

    return this._http.post<CategoryApi>(requestUrl, params).pipe(
      map((data : CategoryApi) => {
        data.data.items.foreach(function (e: any) {
          switch (e.state) {
            case 0:
              e.badgeColor = 'text-gray bg-gray-light'
              break
            case 1:
              e.badgeColor = 'text-green bg-green-light'
              break
            default:
              e.badgeColor = 'text-gray bg-gray-light'
              break
          }
        })
        return data
      })
    )
  }
}
